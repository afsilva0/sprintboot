package com.example.demo.User.controller;


import com.example.demo.User.dto.UserDto;
import com.example.demo.User.service.UserService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;


@RestController
@RequestMapping("user")
public class UserController {

    @Autowired
    private UserService userService;


    @ApiOperation(value="Insert user", response = UserDto.class)
    @ApiResponses( value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Something Went Wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("/create")
    public String create(@RequestBody UserDto userDto) {

        this.userService.saveCustomer(userDto);
        return "guardado";
    }

    @ApiOperation(value="getAll users", response = UserDto.class)
    @ApiResponses( value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Something Went Wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping("getAll")
    public List<UserDto> getAll(){
        return userService.getUsers();
    }


    @ApiOperation(value="getUser by id", response = UserDto.class)
    @ApiResponses( value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Something Went Wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping("getUserById/{id}")
    public Optional<UserDto> getUserById(@PathVariable("id") Long id){
        return userService.getUserById(id);
    }

    @ApiOperation(value="delete user by id", response = UserDto.class)
    @ApiResponses( value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Something Went Wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @GetMapping("deleteUserById/{id}")
    public String deleteUserById(@PathVariable("id") Long id){
        return userService.deleteUserById(id);
    }

    @ApiOperation(value="update user", response = UserDto.class)
    @ApiResponses( value = {
            @ApiResponse(code = 200, message = "OK"),
            @ApiResponse(code = 400, message = "Something Went Wrong"),
            @ApiResponse(code = 500, message = "Internal Server Error")
    })
    @PostMapping("updateUser")
    public String deleteUserById(@RequestBody UserDto userDto){
        return userService.updateUser(userDto);
    }


}
