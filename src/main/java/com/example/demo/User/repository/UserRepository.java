package com.example.demo.User.repository;


import com.example.demo.User.dto.UserDto;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;


public interface UserRepository extends JpaRepository<UserDto,Long> {

    Void save(Optional<UserDto> user);
}
