package com.example.demo.User.dto;


import lombok.Data;

import javax.persistence.*;

@Entity
@Table(name = "USUARIOS")
@Data
public class UserDto {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name="ID")
    Long id;
    @Column(name="USUARIO")
    String usuario;
    @Column(name="CONTRASENA")
    String contrasena;

}
